#include<iostream>
#include<math.h>
#include<algorithm>
#include<sstream>
#include <windows.h>
using namespace std;

//动态规划算法求解，时间复杂度O(n)，空间O(n)
int dpprofit1(int* stock, int n)
{
	int** dp = new int* [n + 1];
	for (int i = 0;i <= n;i++)
		dp[i] = new int[3];
	dp[1][0] = -stock[0];
	dp[1][1] = 0;
	dp[1][2] = 0;
	for (int i = 2;i <=n;i++)
	{
		dp[i][0] = max(dp[i - 1][0], dp[i - 1][2] - stock[i-1]);
		dp[i][1] = dp[i - 1][0] + stock[i-1];
		dp[i][2] = max(dp[i - 1][1], dp[i - 1][2]);
	}
	return max(dp[n][1],dp[n][2]);

}
//动态规划算法优化求解，时间复杂度O(n)，空间O(1)
int dpprofit2(int* stock, int n)
{
	int dp0 = -stock[0];
	int dp1 = 0, dp2 = 0;
	for (int i = 1;i < n;i++)
	{
		int tmp0 = max(dp0, dp2 - stock[i]);
		int tmp1 = dp0 + stock[i];
		int tmp2 = max(dp1, dp2);
		dp0 = tmp0;
		dp1 = tmp1;
		dp2 = tmp2;
	}
	return max(dp1,dp2);

}
int main()
{
	int testnum;
	cout << "请输入需要测试的次数";
	cin >> testnum;
	for (int j = 0;j < testnum;j++)
	{
		long long head, tail, freq;
		int n = 0;
		int num = 0;
		cout << "输入观察股票的天数n: ";
		cin >> n;
		cout << "输入n天的股票价格: ";
		int* stock = new int[n];
		char c;
		cin >> stock[num++];
		while ((c = getchar()) != '\n')
		{
			cin >> stock[num++];
		}
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用动态规划1算法求解，最大利润为： " << dpprofit1(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用动态规划2算法优化求解，最大利润为： " << dpprofit2(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		cout << endl;
	}
	system("pause");
	return 0;

}