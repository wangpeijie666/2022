#include<iostream>
#include<math.h>
#include<algorithm>
#include<sstream>
#include <windows.h>
using namespace std;

//贪心算法求解，时间复杂度O(n)
int greedyprofit(int* stock, int n)
{
	int maxprofit = 0;
	if (n < 2) return maxprofit;
	for (int i = 1;i < n;i++)
	{
		if (stock[i] > stock[i - 1])
			maxprofit += stock[i] - stock[i - 1];
	}
	return maxprofit;
}
//动态规划算法求解，时间复杂度O(n)，空间O(n^2)
int dpprofit1(int* stock, int n)
{
	if (stock == NULL || n < 2)
		return 0;
	int**dp = new int*[n];//用来存放当日的最大利润
	for (int i = 0;i < n;i++)
	{
		dp[i] = new int[n];
	}
	dp[0][0] = 0;
	dp[0][1] = 0-stock[0];
	for (int i = 1;i < n;i++)
	{
		dp[i][0] = max(dp[i - 1][0], stock[i]+dp[i-1][1]);
		dp[i][1] = max(dp[i - 1][1], -stock[i] + dp[i - 1][0]);
	}
	return dp[n - 1][0];

}
//动态规划算法优化求解，时间复杂度O(n)，空间O(n)
int dpprofit2(int* stock, int n)
{
	int maxprofit1 = 0 - stock[0];
	int maxprofit0 = 0;
	if (stock == NULL || n < 2)
		return 0;
	for (int i = 1;i < n;i++)
	{
		maxprofit0= max(maxprofit0, maxprofit1+stock[i]);
		maxprofit1 = max(maxprofit1, maxprofit0 - stock[i]);
	}
	return maxprofit0;

}
int main()
{
	int testnum;
	cout << "请输入需要测试的次数";
	cin >> testnum;
	for (int j = 0;j < testnum;j++)
	{
		long long head, tail, freq;
		int n = 0;
		int num = 0;
		cout << "输入观察股票的天数n: ";
		cin >> n;
		cout << "输入n天的股票价格: ";
		int* stock = new int[n];
		char c;
		cin >> stock[num++];
		while ((c = getchar()) != '\n')
		{
			cin >> stock[num++];
		}
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用贪心算法求解，最大利润为： " << greedyprofit(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用动态规划1算法求解，最大利润为： " << dpprofit1(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用动态规划2算法优化求解，最大利润为： " << dpprofit2(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		cout << endl;
	}
	system("pause");
	return 0;

}