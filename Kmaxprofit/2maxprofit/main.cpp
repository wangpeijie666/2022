#include<iostream>
#include<math.h>
#include<algorithm>
#include<sstream>
#include <windows.h>
using namespace std;

//贪心算法求解，时间复杂度O(n),在动态规划中会使用到
int greedyprofit(int* stock, int n)
{
	int maxprofit = 0;
	if (n < 2)return maxprofit;
	for (int i = 1;i < n;i++)
	{
		if (stock[i] > stock[i - 1])
			maxprofit += stock[i] - stock[i - 1];
	}
	return maxprofit;
}
//动态规划优化算法求解，时间复杂度O(n)，空间O(n)
int dpprofitk(int* stock, int n,int k)
{
	if (n==0) return 0;
	if (k > n / 2)//如果可交易次数足够多大于总天数一半,这时相当于无限次
		return greedyprofit(stock, n);
	int* dp = new int[k+1];
	int* lowest = new int[k + 1];
	for (int i = 1;i <=k;i++)
	{
		lowest[i] = stock[0];
	}
	for(int i=1;i<n;i++)
		for (int j = 1;j <= k;j++)
		{
			lowest[j] = min(stock[i] - dp[j - 1], lowest[j]);
			dp[j] = max(dp[j], stock[i] - lowest[j]);
		}
	return dp[k];

}

int main()
{
	int testnum;
	cout << "请输入需要测试的次数";
	cin >> testnum;
	int k;
	cout << "请输入可以交易的次数";
	cin >> k;
	for (int j = 0;j < testnum;j++)
	{
		long long head, tail, freq;
		int n = 0;
		int num = 0;
		cout << "输入观察股票的天数n: ";
		cin >> n;
		cout << "输入n天的股票价格: ";
		int* stock = new int[n];
		char c;
		cin >> stock[num++];
		while ((c = getchar()) != '\n')
		{
			cin >> stock[num++];
		}
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用动态规划k算法优化求解，最大利润为： " << dpprofitk(stock, n,k) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		cout << endl;
	}
	system("pause");
	return 0;

}