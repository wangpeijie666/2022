#include<iostream>
#include<math.h>
#include<algorithm>
#include<sstream>
#include <windows.h>
using namespace std;

//普通方法求解，时间复杂度O(n2)
int normalprofit(int* stock, int n)
{
	int maxprofit = 0;
	for (int i = 0;i < n;i++)
		for (int j = i + 1;j < n;j++)
			maxprofit = max(maxprofit, stock[j] - stock[i]);
	return maxprofit;
}
//分治法求解，时间复杂度O(nlogn)
int merprofit(int* stock, int l, int r)
{
	if (l >= r)
		return 0;
	int mid = (l + r) / 2;
	int x1 = merprofit(stock, l, mid - 1);
	int x2 = merprofit(stock, mid + 1, r);
	int lmin = stock[mid];
	int rmax = stock[mid];
	for (int i = mid;i >= 0;i--)
		lmin = min(stock[i], lmin);
	for (int i = mid;i <= r;i++)
		rmax = max(rmax, stock[i]);
	int x3 = rmax - lmin;
	return max(max(x1, x2), x3);
}
int diviprofit(int* stock, int n)
{
	return merprofit(stock, 0, n - 1);
}
//贪心算法求解，时间复杂度O(n)
int greedyprofit(int* stock, int n)
{
	int lowest = stock[0];
	int maxprofit = 0;
	for (int i = 0;i < n;i++)
	{
		if (stock[i] < lowest)
			lowest = stock[i];
		maxprofit = max(maxprofit, stock[i] - lowest);
	}
	return maxprofit;
}
//动态规划算法求解，时间复杂度O(n)，空间O(n)
int dpprofit1(int* stock, int n)
{
	if (stock == NULL || n < 2)
		return 0;
	int* dp = new int[n];//用来存放当日的最大利润
	dp[0] = 0;
	int lowest = stock[0];//用来存放之前的最低价格
	for (int i = 1;i < n;i++)
	{
		dp[i] = max(dp[i - 1], stock[i] - lowest);
		lowest = min(lowest, stock[i]);
	}
	return dp[n - 1];

}
//动态规划算法优化求解，时间复杂度O(n)，空间O(1)
int dpprofit2(int* stock, int n)
{
	int dp = 0;//用来记录所有中的最大利润
	if (stock == NULL || n < 2)
		return dp;
	int lowest = stock[0];//用来存放之前的最低价格
	for (int i = 1;i < n;i++)
	{
		dp= max(dp, stock[i] - lowest);
		lowest = min(lowest, stock[i]);
	}
	return dp;

}
int main()
{
	int testnum;
	cout << "请输入需要测试的次数";
	cin >> testnum;
	for (int j = 0;j < testnum;j++)
	{
		long long head, tail, freq;
		int n = 0;
		int num = 0;
		cout << "输入观察股票的天数n: ";
		cin >> n;
		cout << "输入n天的股票价格: ";
		int* stock = new int[n];
		char c;
		cin >> stock[num++];
		while ((c = getchar()) != '\n')
		{
			cin >> stock[num++];
		}
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用普通算法求解，最大利润为： " << normalprofit(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用分治算法求解，最大利润为： " << diviprofit(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用贪心算法求解，最大利润为： " << greedyprofit(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用动态规划1算法求解，最大利润为： " << dpprofit1(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		cout << "利用动态规划2算法优化求解，最大利润为： " << dpprofit2(stock, n) << endl;
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		cout << "时间为" << (tail - head) * 1000.0 / freq << "ms" << endl;

		cout << endl;
	}
	system("pause");
	return 0;

}